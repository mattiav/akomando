const webpack = require('webpack');
const path = require('path');
const libraryName = 'akomando';

module.exports = {
   entry: path.resolve(__dirname, 'src/bin/akomando.js'),
   output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.akomando.bin.js',
      library: libraryName,
      libraryTarget: 'umd',
      umdNamedDefine: true
   },

   mode: 'production',
   target: 'node',

   module: {
      rules: [{
         test: /\.js$/,
         include: path.resolve(__dirname, 'src'),
         use: ['shebang-loader',
         {
            loader: 'babel-loader'
         }]
      }]
   },

   plugins: [
      new webpack.BannerPlugin({ banner: "#!/usr/bin/env node", raw: true })
   ]
};
